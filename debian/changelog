ignition-cmake (2.11.0-1) unstable; urgency=medium

  * New upstream version 2.11.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Mon, 07 Mar 2022 16:01:20 +0000

ignition-cmake (2.10.0-2) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Add pkg-config depends, needed for Find*.cmake
  * Bump policy versions (no changes)
  * Drop old Replaces/Breaks (version not in archive)
  * Bump copyright
  * Drop gitlab-ci

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 03 Feb 2022 14:18:37 +0000

ignition-cmake (2.10.0-1) unstable; urgency=medium

  * New upstream version 2.10.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Fri, 28 Jan 2022 15:59:03 +0000

ignition-cmake (2.9.0-1) unstable; urgency=medium

  * New upstream version 2.9.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Wed, 06 Oct 2021 15:34:38 +0000

ignition-cmake (2.8.0-2) unstable; urgency=medium

  * Upload to unstable

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Mon, 23 Aug 2021 15:01:30 +0000

ignition-cmake (2.8.0-1) experimental; urgency=medium

  * Update patch to keep same behaviour
  * Missing field in copyright. Fix Upstream-Name
  * New upstream version 2.8.0
  * Update patches to new version
  * Remove buildsystem=cmake from rules

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 03 Jun 2021 14:37:52 +0000

ignition-cmake (2.7.0-1) unstable; urgency=medium

  * New upstream version 2.7.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Fri, 30 Apr 2021 16:03:07 +0000

ignition-cmake (2.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.6.2

 -- Timo Röhling <timo@gaussglocke.de>  Fri, 22 Jan 2021 12:03:25 +0100

ignition-cmake (2.6.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.6.1
  * Bump Standards-Version to 4.5.1 (no changes)
  * Bump debhelper compat to 13 (no changes)
  * Set proper hashbang for installed Python scripts

 -- Timo Röhling <timo@gaussglocke.de>  Tue, 15 Dec 2020 17:42:26 +0100

ignition-cmake (2.5.0-2) unstable; urgency=medium

  * Team upload.
  * Do not override unit test default timeout.
    On some buildd machines, 240 seconds is too short for the example build
    test of ignition-math. The default value (1500 seconds) is fine.

 -- Timo Röhling <timo@gaussglocke.de>  Thu, 26 Nov 2020 12:04:16 +0100

ignition-cmake (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 10 Sep 2020 19:21:59 +0000

ignition-cmake (2.2.0-4) unstable; urgency=medium

  * Team upload.
  * Convert to arch all

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 19 Apr 2020 12:04:15 +0200

ignition-cmake (2.2.0-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 18 Apr 2020 11:29:22 +0200

ignition-cmake (2.2.0-2) experimental; urgency=medium

  * Team upload.
  * Update d/watch to Github
  * Drop ruby-ronn (Closes: #903284)
  * Drop python2 (Closes: #936725)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 18 Apr 2020 11:22:03 +0200

ignition-cmake (2.2.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 2.2.0
  * Update packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 14 Apr 2020 19:02:13 +0200

ignition-cmake2 (2.1.1+dfsg-3) unstable; urgency=medium

  * Fix install file. Closes: #947441

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Fri, 10 Jan 2020 15:52:30 +0000

ignition-cmake2 (2.1.1+dfsg-2) unstable; urgency=medium

  [ Jose Luis Rivero ]
  * Use Comment tag in copyright
  * Do not to repack sources, tar.gz2 preferred

  [ Jochen Sprickerhof ]
  * Simplify comment in d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 25 Dec 2019 22:33:23 +0100

ignition-cmake2 (2.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2.1.1
    (Closes: #944539)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Mon, 11 Nov 2019 15:46:23 +0000

ignition-cmake (0.6.1-1) unstable; urgency=medium

  * New upstream version 0.6.1
  * Include examples in -dev file
  * Remove ruby-ronn build-dep (Closes: #903284)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 18 Oct 2018 15:27:37 +0000

ignition-cmake (0.4.1-1) unstable; urgency=medium

  * New upstream version 0.4.1

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 01 Mar 2018 18:40:30 +0000

ignition-cmake (0.4.0-1) unstable; urgency=medium

  * New upstream version 0.4.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 16 Jan 2018 23:32:15 +0000

ignition-cmake (0.3.0+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 0.3.0

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 09 Jan 2018 15:45:19 +0000

ignition-cmake (0.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #882237)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 05 Dec 2017 21:05:14 +0100
